const fs = require('fs');
const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const settings = {
  title: 'Best Show',
  season: '1',
  startIndex: '1',
  directory: '/Users/tyler/Projects/soder/season01',
}

const { title, directory } = settings;
const startIndex = Number(settings.startIndex);
const season = Number(settings.season);

const formatNumber = (num) => (num).toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping:false});

const getFiles = (path) => {
  return new Promise((resolve, reject) => {
    fs.readdir(path, (err, files) => {
      resolve(files.sort((a, b) => {
        return a.localeCompare(b, undefined, {numeric: true, sensitivity: 'base'});
      }));
    });
  });
}

const renameFile = (fileName, index) => {
  const ext = fileName.match(/\..+$/)[0];
  const episodeNum = formatNumber(startIndex + index);
  const seasonNum = formatNumber(season);
  const newFileName = `${directory}/${title} - s${seasonNum}e${episodeNum} - ${ext}`
  return new Promise((resolve, reject) => {
    fs.rename(`${directory}/${fileName}`, newFileName, (err) => {
      if (err) throw err;
      resolve(newFileName);
    });
  });
}

(init = async () => {
  const files = await getFiles(directory)
  files.forEach(file => console.log(file));
  const answer = await new Promise((resolve, reject) => {
    rl.question('These files will be overwritten in order. Continue? (no/yes)', (answer) => {
      rl.close()
      resolve(answer);
    });
  })
  if (answer !== 'yes') return;
  const renamedFiles = await Promise.all(files.map((file, i) => renameFile(file, i)));
  console.log(renamedFiles, 'Your files have been renamed');
})();
